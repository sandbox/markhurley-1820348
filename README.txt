/*
*  Author   : Mark Hurley  <mark@pioneeringsoftware.com>
*  Subject  : README file for module uc_braintree_tr_payment
*/

This module utilizes Braintree's transparent redirection technique to transmit 
credit card information from the user's browser directly to the Braintree vault. 
Tokens are substituted for credit card data within the merchant's website to 
avoid costly audits related to the storage or transmission of credit card
information. 

This module provides users with the ability to manage payment tokens via the
last four card digits, identify and update expired cards, schedule recurring
billing, change billing dates, and cancel recurring payments for subscriptions
processed via the Braintree Payment Solution.
 
The module includes an import methodology which enables a merchant to quickly 
synchronize with customer, token, and subscription information contained in both 
Drupal and the Braintree vault, providing an easy transition to the Braintree 
payment gateway or as a recovery option for business continuity. There is a
limitation with the number of search results which Braintree will provide at
this time which is also the maximum number for results for the recovery option.
The cap, as of August 2013 is 10,000 values.  For more information,
please see https://www.braintreepayments.com/docs/php/general/search_results.

The module also supports uc_roles, allowing one or more roles to be assigned
to a configured product subscription. Roles may also be automatically revoked
with cancelation of a subscription or an expired credit card.

Please see the INSTALL.txt for information related to the installation of this
module.
