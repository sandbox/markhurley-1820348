<?php
/**
 * @file
 * Used to cancel a Payflow pro recurring order if a like substitute has been
 * configured within the settings.php skumap.
 */

/**
 * Function to cancel a payflowpro subscription based on settings.ph skumap.
 */
function uc_braintree_tr_payment_cancel_payflowpro_model($order, $sku_map) {

  if (module_exists('uc_payflowpro')) {

    uc_braintree_tr_payment_log_debug_message(t("uc_payflowpro module exists"));
    uc_braintree_tr_payment_log_debug_message(t("order product count: %count",
        array('%count' => count($order->products))));
    uc_braintree_tr_payment_log_debug_message(t("skuMap entries: %count",
        array('%count' => count($sku_map))));

    foreach ($order->products as $key) {
      if (isset($sku_map[$key->model])) {

        $placeholders = db_placeholders($sku_map[$key->model]);

        // Steps to execute to invoke the profile cancelation.
        require_once drupal_get_path('module', 'uc_payflowpro') . "/" .
            'uc_payflowpro.ca.inc';
        require_once drupal_get_path('module', 'uc_payflowpro') . "/" .
            'includes/payflowpro_utils.inc';
        require_once drupal_get_path('module', 'uc_payflowpro') . "/" .
            'includes/PayflowProRecurring.class.php';
        require_once drupal_get_path('module', 'uc_payflowpro') . "/" .
            'includes/uc_payflowpro_recurring.inc';

        $pfp_profiles = array();

        $result = db_query("SELECT pfp_profile_id FROM {uc_payflowpro_recurring_profiles} WHERE pfp_uid = %d and uc_model in ($placeholders) and profile_status = 'ACTIVE'",
            $order->uid, $sku_map[$key->model]);

        while ($row = db_fetch_object($result)) {
          $pfp_profiles[] = $row->pfp_profile_id;
        }

        uc_braintree_tr_payment_log_debug_message(t("Vendor: %vendor  Partner %partner  User %user",
            array(
              '%vendor' => variable_get('uc_payflowpro_vendor', ''),
              '%partner' => variable_get('uc_payflowpro_partner', ''),
              '%user' => variable_get('uc_payflowpro_user', ''),
            ))
        );

        if (count($pfp_profiles) > 0) {
          foreach ($pfp_profiles as $profile_id) {
            uc_braintree_tr_payment_log_debug_message(t("Canceling Payflow Pro profile %profile for uid %uid since a Braintree order for model %model will replace the current recurring payment.",
                array(
                  '%profile' => $profile_id,
                  '%uid' => $order->uid,
                  '%model' => $key->model,
                ))
            );

            $profile = new PayflowProRecurring($profile_id, _uc_payflowpro_get_auth());
            $profile->refresh();
            $try = $profile->cancel();
            if ($try == 0) {
              uc_braintree_tr_payment_log_debug_message(t("Cancelation returned false for profile %profile_id",
                  array('%profile_id' => $profile_id)));
              uc_braintree_tr_payment_log_debug_message(t("Code for profile %profile_id: %return_code",
                  array(
                    '%profile_id' => $profile_id,
                    '%return_code' => $profile->getReturnCode(),
                  ))
              );
              uc_braintree_tr_payment_log_debug_message(t("Return Message for profile %profile_id: %return_message",
                  array(
                    '%profile_id' => $profile_id,
                    '%return_message' => $profile->getReturnMsg(),
                  ))
              );
            }
            else {
              uc_braintree_tr_payment_log_debug_message(t("Cancelation returned TRUE for profile %profile_id",
                  array('%profile_id' => $profile_id))
              );
            }
          }
        }
        else {
          uc_braintree_tr_payment_log_debug_message(t("A Payflo Pro profile ID could not be found for user id %uid. No cancelation was performed.",
              array('uid' => $order->uid)));
        }
      }
    }
  }
}
