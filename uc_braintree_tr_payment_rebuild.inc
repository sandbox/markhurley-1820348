<?php
/**
 * @file
 * Used to rebuild Braintree customer, token, and subscription information
 * locally within the Drupal installation.
 */

/**
 * Function to import customer, token, and subscription from the Braintree API.
 */
function uc_braintree_tr_payment_rebuild_tables($sku_role_id) {

  uc_braintree_tr_payment_log_debug_message(t("Rebuilding uc_braintree_tr_payment tables."));

  // Build a hash of the email to uid.
  $email_uid = uc_braintree_tr_payment_get_email_uid();

  // Build a hash of the email to the Braintree Customer Id, Customer ID
  // and tokens, and Customer Id and subscriptions.
  $results = uc_braintree_tr_payment_get_braintree_customer_token_subscription();
  $email_customer_id = $results['0'];
  $customer_id_tokens = $results['1'];
  $customer_id_subscriptions = $results['2'];

  // Build a hash of the uid to customer ID.
  $uid_customer_id = uc_braintree_tr_payment_get_uid_customer_id($email_uid, $email_customer_id);

  // Build a hash of the customer ID to uid.
  $customer_id_uid = uc_braintree_tr_payment_get_customer_id_uid($email_uid, $email_customer_id);

  // Insert/update uc_braintree_tr_payment_customer.
  uc_braintree_tr_payment_rebuild_table_customer($uid_customer_id);

  // Insert/update uc_braintree_tr_payment_token.
  uc_braintree_tr_payment_rebuild_table_payment_token($customer_id_tokens, $customer_id_uid);

  // Insert/update uc_braintree_tr_payment_customer_subscription.
  uc_braintree_tr_payment_rebuild_table_customer_subscription($customer_id_subscriptions, $customer_id_uid);

  // Apply product roles.
  uc_braintree_tr_payment_reapply_roles($customer_id_subscriptions, $customer_id_uid, $sku_role_id);

  // Get the Braintree subscription plan names.
  $plans = uc_braintree_tr_payment_get_braintree_subscription_plans();

  // Insert/update uc_braintree_tr_payment_subscription.
  uc_braintree_tr_payment_write_subscriptions($plans);

}

/**
 * Function used to import/update customer information in to the customer table.
 */
function uc_braintree_tr_payment_rebuild_table_customer($uid_customer_id) {
  foreach ($uid_customer_id as $uid => $customer_id) {

    // Do not store a customer with an empty uid.
    if (!($uid)) {
      continue;
    }

    // Do not store a customer with an empty customer id.
    if (!($customer_id)) {
      return;
    }

    db_query("INSERT into {uc_braintree_tr_payment_customer} (uid, customer_id, last_modified) values ('%d', '%s', UNIX_TIMESTAMP(now())) on duplicate key UPDATE last_modified = UNIX_TIMESTAMP(now())",
      $uid, $customer_id);
  }
}

/**
 * Function used to import/update token information in to the token table.
 */
function uc_braintree_tr_payment_rebuild_table_payment_token($customer_id_tokens, $customer_id_uid) {

  foreach ($customer_id_tokens as $customer_id => $value) {

    $uid = $customer_id_uid[$customer_id];

    if ($uid) {

      foreach ($customer_id_tokens[$customer_id] as $token => $t_value) {

        // Do not store a token with an empty uid.
        if (!($uid)) {
          continue;
        }

        db_query("INSERT into {uc_braintree_tr_payment_token} (uid, token, card_type, last_four, active, last_modified, status) values ('%d', '%s', '%s', '%s', '1', UNIX_TIMESTAMP(now()), '%s') on duplicate key UPDATE last_modified = UNIX_TIMESTAMP(now()),active = '1', status = '%s', uid = '%d'",
          $uid, $token, $t_value['cardType'], $t_value['last4'],
          $t_value['status'], $t_value['status'], $uid);
      }
    }
  }
}

/**
 * Function used to import/update customer subscription information.
 */
function uc_braintree_tr_payment_rebuild_table_customer_subscription($customer_id_subscriptions, $customer_id_uid) {

  foreach ($customer_id_subscriptions as $customer_id => $value) {
    $uid = $customer_id_uid[$customer_id];
    if ($uid) {
      foreach ($customer_id_subscriptions[$customer_id] as $subscription_id => $s_value) {

        $token = $s_value['token'];
        $status = $s_value['status'];
        $plan_id = $s_value['planId'];

        // Do not store a subscription with an empty uid.
        if (!($uid)) {
          continue;
        }

        db_query("INSERT into {uc_braintree_tr_payment_customer_subscription} (uid, token,
          subscription_id, plan_id, status, last_modified, transaction_id) " .
          " values ('%d', '%s', '%s', '%s', '%s', UNIX_TIMESTAMP(now()), '0') " .
          "on duplicate key UPDATE last_modified = UNIX_TIMESTAMP(now()), status = '%s', token = '%s', plan_id = '%s', uid = '%d'",
          $uid, $token, $subscription_id, $plan_id, $status, $status, $token, $plan_id, $uid);

      }
    }
  }

}
