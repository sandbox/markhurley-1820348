<?php
/**
 * @file
 * Include file used to populate the sandbox for benchmark testing.
 */

/**
 * Function used to generate sample Drupal users and related Braintree tokens.
 */
function uc_braintree_tr_payment_populate_sandbox() {

  $accounts = 2000;
  $create_count = 0;
  $domain = "sandbox.org";
  $zip_code = "33333";
  $characters = "abcdefghijklmnopqrstuvwxyz";

  $test_card_data = array(
    '1' => array('number' => '378282246310005', 'cvv' => '1234'),
    '2' => array('number' => '5555555555554444', 'cvv' => '123'),
    '3' => array('number' => '4111111111111111', 'cvv' => '123'),
  );

  $sandbox_users = array();

  while (count($sandbox_users) < $accounts) {
    $username = '';
    for ($i = 0; $i < 8; $i++) {
      $username .= $characters[rand(0, drupal_strlen($characters))];
    }

    $rand = rand(1, count($test_card_data));
    $sandbox_users[$username] = array(
      'email' => $username . "@" . $domain,
      'number' => $test_card_data[$rand]['number'],
      'cvv' => $test_card_data[$rand]['cvv'],
      'zip_code' => $zip_code,
    );
  }

  if (!(uc_braintree_tr_payment_configuration_initialization())) {
    return;
  }

  $plans = Braintree_Plan::all();

  if (count($plans) > 0) {
    foreach ($plans as $plan) {
      $plan_id = $plan->_attributes['id'];
      break;
    }
  }

  $website = variable_get('uc_braintree_tr_payment_website', '');

  foreach ($sandbox_users as $username => $value) {
    try {
      $result = Braintree_Customer::create(
          array(
            'firstName' => $username,
            'email' => $value['email'],
            'website' => $website,
            'creditCard' => array(
              'number' => $value['number'],
              'expirationDate' => '05/14',
              'cardholderName' => 'The Cardholder',
              'cvv' => $value['cvv'],
              'billingAddress' => array(
                'postalCode' => $value['zip_code'],
              ),
            ),
          )
      );

      if ($result->success) {
        $create_count++;

        if ($plan_id) {
          $token = $result->customer->creditCards[0]->token;

          try {
            $next_month = new DateTime("now + 1 month");
            $next_month->setTime(0, 0, 0);
            $s_result = Braintree_Subscription::create(
                array(
                  'paymentMethodToken' => $token,
                  'planId' => $plan_id,
                  'firstBillingDate' => $next_month,
                  'price' => '10.00',
                )
            );

          }
          catch (Exception $e) {
            foreach ($s_result->errors->deepAll() as $error) {
              uc_braintree_tr_payment_log_debug_message(t("%error_code: %error_message",
                  array(
                    '%error_code' => $error->code,
                    '%error_message' => $error->message,
                  ))
              );
            }
          }
        }

        $new_user = array(
          'name' => $username,
          'pass' => $username,
          'mail' => $value['email'],
          'init' => $value['email'],
          'status' => 1,
          'access' => REQUEST_TIME,
        );

        user_save(NULL, $new_user);

      }
      else {
        foreach ($result->errors->deepAll() as $error) {
          uc_braintree_tr_payment_log_debug_message(t("%error_code: %error_message",
              array(
                '%error_code' => $error->code,
                '%error_message' => $error->message,
              ))
          );
        }
      }
    }
    catch (Exception $e) {
      uc_braintree_tr_payment_connection_exception($e);
    }

  }
  return $create_count;
}
