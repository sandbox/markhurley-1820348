<?php
/**
 * @file
 * Include file used to respond to processor decline and validation errors.
 */

/**
 * Function containing error handling based on Braintree response messages.
 */
function uc_braintree_tr_payment_redirect_error_handling($result) {

  /* De-duplicate and include the Braintree transaction validation errors
   * based on the web form values. For security reasons, do not include the
  * processor decline messages.
  *
  * Braintree Responses:
  * http://www.braintreepaymentsolutions.com/docs/php/transactions/validations
  *
  * Processor Decline Responses:
  * http://www.braintreepaymentsolutions.com
  * Under /docs/php/reference/processor_responses
  */

  $validation_errors = array(
    81501 => "Amount cannot be negative.",
    81502 => "Amount is required.",
    81503 => "Amount is an invalid format.",
    81509 => "Credit card type is not accepted by this merchant account.",
    81520 => "Processor authorization code must be 6 characters.",
    81527 => "Custom field is too long.",
    81528 => "Amount is too large.",
    81531 => "Amount must be greater than zero.",
    81534 => "Tax amount cannot be negative.",
    81535 => "Tax amount is an invalid format.",
    81536 => "Tax amount is too large.",
    81601 => "Company is too long.",
    81603 => "Custom field is too long.",
    81604 => "Email is an invalid format.",
    81605 => "Email is too long.",
    81606 => "Email is required if sending a receipt.",
    81607 => "Fax is too long.",
    81608 => "First name is too long.",
    81613 => "Last name is too long.",
    81614 => "Phone is too long.",
    81615 => "Website is too long.",
    81616 => "Website is an invalid format.",
    81703 => "Credit card type is not accepted by this merchant account.",
    81706 => "CVV is required.",
    81707 => "CVV must be 3 or 4 digits.",
    81709 => "Expiration date is required.",
    81710 => "Expiration date is invalid.",
    81711 => "Expiration date year is invalid.",
    81712 => "Expiration month is invalid.",
    81713 => "Expiration year is invalid.",
    81714 => "Credit card number is required.",
    81715 => "Credit card number is invalid.",
    81716 => "Credit card number must be 12-19 digits.",
    81717 => "Credit card number is not an accepted test number.",
    81718 => "Credit card number cannot be updated to an unsupported card type when it is associated to subscriptions.",
    81723 => "Cardholder name is too long.",
    81801 => "Address must have at least one field fill in.",
    81802 => "Company is too long.",
    81804 => "Extended address is too long.",
    81805 => "First name is too long.",
    81806 => "Last name is too long.",
    81807 => "Locality is too long.",
    81808 => "Postal code is required.",
    81809 => "Postal code may contain no more than 9 letter or number characters.",
    81810 => "Region is too long.",
    81811 => "Street address is required.",
    81812 => "Street address is too long.",
    81813 => "Postal code can only contain letters, numbers, spaces, and hyphens.",
    91501 => "Order ID is too long.",
    91504 => "Transaction can only be voided if status is authorized or submitted_for_settlement.",
    91505 => "Cannot refund credit.",
    91506 => "Cannot refund a transaction unless it is settled.",
    91507 => "Cannot submit for settlement unless status is authorized.",
    91508 => "Need a customer_id, payment_method_token, credit_card, or subscription_id.",
    91510 => "Customer ID is invalid.",
    91511 => "Customer does not have any credit cards.",
    91512 => "Transaction has already been refunded.",
    91513 => "Merchant account ID is invalid.",
    91514 => "Merchant account is suspended.",
    91515 => "Cannot provide both payment_method_token and credit_card attributes.",
    91516 => "Cannot provide both payment_method_token and customer_id unless the payment_method belongs to the customer.",
    91517 => "Credit card type is not accepted by this merchant account.",
    91518 => "Payment method token is invalid.",
    91519 => "Processor authorization code cannot be set unless for a voice authorization.",
    91521 => "Refund amount cannot be more than the authorized amount.",
    91522 => "Settlement amount cannot be more than the authorized amount.",
    91523 => "Transaction type is invalid.",
    91524 => "Transaction type is required.",
    91525 => "Vault is disabled.",
    91526 => "Custom field is invalid.",
    91527 => "Cannot provide both payment_method_token and subscription_id unless the payment method belongs to the subscription.",
    91528 => "Subscription ID is invalid.",
    91529 => "Cannot provide both subscription_id and customer_id unless the subscription belongs to the customer.",
    91530 => "Cannot provide a billing address unless also providing a credit card.",
    91531 => "Subscription status must be past due.",
    91537 => "Purchase order number is too long.",
    91538 => "Cannot refund transaction with suspended merchant account.",
    91602 => "Custom field is invalid.",
    91609 => "Customer ID has already been taken.",
    91610 => "Customer ID is invalid.",
    91611 => "Customer ID is not an allowed ID.",
    91612 => "Customer ID is too long.",
    91613 => "Id is required",
    91701 => "Cannot provide both a billing address and a billing address ID.",
    91702 => "Billing address ID is invalid.",
    91704 => "Customer ID is required.",
    91705 => "Customer ID is invalid.",
    91708 => "Cannot provide expiration_date if you are also providing expiration_month and expiration_year.",
    91718 => "Token is invalid.",
    91719 => "Credit card token is taken.",
    91720 => "Credit card token is too long.",
    91721 => "Token is not an allowed token.",
    91722 => "Payment method token is required.",
    91723 => "Update Existing Token is invalid.",
    91803 => "Country name is not an accepted country.",
    91814 => "Country code alpha-2 is not accepted",
    91815 => "Inconsistent country",
    91816 => "Country code alpha-3 is not accepted",
    91817 => "Country code numeric is not accepted",
    91818 => "Too many addresses per customer",
  );

  $response = array();

  if ((!$result->success and $result->transaction->processorResponseCode < 2000) or
      (!$result->success and !$result->transaction->processorResponseCode)) {

    if ($result->creditCardVerification->status == "processor_declined") {
      uc_braintree_tr_payment_log_debug_message(t("%response_code %response_text",
          array(
            '%response_code' => $result->creditCardVerification->processorResponseCode,
            '%response_text' => $result->creditCardVerification->processorResponseText,
          ))
      );
    }

    foreach ($result->errors->deepAll() as $error) {
      $validation_error_codes[$error->code] = 1;
    }

    if ($validation_error_codes) {
      foreach ($validation_error_codes as $error => $value) {
        if ($validation_errors[$error]) {
          $response[] = $validation_errors[$error];
        }
      }
    }
  }

  $output = "<br/>" .
      ($result->transaction->processorResponseCode > 1999 ? t("The credit card information you have provided has been declined.") :
          t("The credit card information provided could not be processed.")) .
          (count($response) > 0 ? "<ul><li>" . implode("</li><li>", $response) . "</li></ul>" : "&nbsp;") .
          t("Please go back and verify your credit card information.") . "<br/><br/>" .
          drupal_get_form('uc_braintree_tr_payment_back_form');

  return $output;
}
