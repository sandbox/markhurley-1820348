<?php
/**
 * @file
 * Contains admin settings for the uc_braintree_tr_payment module
 *
 *    Author   : Mark Hurley  <mark@pioneeringsoftware.com>
 *    Subject  : Module used to perform Braintree transparent redirect credit
 *    card processing and server-to-server token transaction processing. Module
 *    also supports Braintree subscription management and role management.
 */

/**
 * Function used to call the appropriate Braintree Configuration functions.
 */
function uc_braintree_tr_payment_store_admin($arg1 = NULL, $arg2 = NULL) {

  switch ($arg1) {
    case 'settings':
      return drupal_get_form("uc_braintree_tr_payment_payment_settings_form");

    case 'tools':
      return uc_braintree_tr_payment_admin_store_braintree_tools($arg2);

    case 'reports':
      return uc_braintree_tr_payment_admin_store_braintree_reports($arg2);

    default:
      return "You may choose from the menu tabs above to configure the Braintree " .
          "settings, generate reports, or execute a tool used to support the " .
          "Braintree TR Payment module.";
  }
}

/**
 * Function used to build the Braintree Configuration settings form.
 */
function uc_braintree_tr_payment_payment_settings_form(&$form_state) {

  uc_braintree_tr_payment_setup_timezone();

  $form = array();

  $form['environment'] = array(
    '#type' => 'select',
    '#title' => t('Environment'),
    '#default_value' => variable_get('uc_braintree_tr_payment_environment',
    'sandbox'),
    '#options' => array(
      'development' => t('Development'),
      'sandbox' => t('Sandbox'),
      'production' => t('Production'),
      'qa' => t('QA'),
    ),
    '#description' => t('Braintree environment to use to execute transactions.'),
  );

  $form['merchant_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Merchant ID'),
    '#default_value' => variable_get('uc_braintree_tr_payment_merchant_id',
    'Please supply a Merchant ID'),
    '#tree' => TRUE,
  );

  $form['public_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Public Key'),
    '#default_value' => variable_get('uc_braintree_tr_payment_public_key',
    'Please supply a Public Key'),
    '#tree' => TRUE,
  );

  $form['private_key_file'] = array(
    '#type' => 'textfield',
    '#title' => t('Private Key File'),
    '#default_value' => variable_get('uc_braintree_tr_payment_private_key_file',
    '/etc/braintree/uc_braintree_tr_payment_private_key'),
    '#tree' => TRUE,
  );

  $form['cvv_rules_match'] = array(
    '#type' => 'checkbox',
    '#title' => t('CVV Rules Match'),
    '#default_value' => variable_get('uc_braintree_tr_payment_cvv_rules_match', NULL),
    '#tree' => TRUE,
  );

  $form['avs_rules_match_postal_code'] = array(
    '#type' => 'checkbox',
    '#title' => t('AVS Rules Postal Code Match'),
    '#default_value' => variable_get('uc_braintree_tr_payment_avs_rules_match_postal_code', NULL),
    '#tree' => TRUE,
  );

  $form['verify_card_temporary_transaction'] = array(
    '#type' => 'checkbox',
    '#title' => t('Verify New Card with Temporary Transaction'),
    '#default_value' => variable_get('uc_braintree_tr_payment_verify_card_temporary_transaction', NULL),
    '#tree' => TRUE,
  );

  $form['transaction_debugging'] = array(
    '#type' => 'checkbox',
    '#title' => t('Transaction Debugging'),
    '#default_value' => variable_get('uc_braintree_tr_payment_transaction_debugging', NULL),
    '#tree' => TRUE,
  );

  $form['website'] = array(
    '#type' => 'textfield',
    '#title' => t('Website Identifier (Fully Qualified Domain Name; Left Blank Retrieves All)'),
    '#default_value' => variable_get('uc_braintree_tr_payment_website', ''),
    '#tree' => TRUE,
  );

  // Removed the option to rebuild the information via the cron due to possible
  // problems with the number of entries in the sandbox.
  // $form['rebuild_module_tables'] = array(
  // '#type' => 'checkbox',
  // '#title' => t('Rebuild Customer, Subscription, and Token Information
  // from the Braintree API (Cron Scheduled)'),
  // '#default_value' =>
  // variable_get('uc_braintree_tr_payment_rebuild_module_tables', NULL),
  // '#tree' => TRUE,
  // );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Update'),
    '#suffix' => l(t('Cancel'), 'admin/store/payments/braintree/settings'),
  );

  return $form;
}

/**
 * Function used to validate the Braintree Configuration form settings.
 */
function uc_braintree_tr_payment_payment_settings_form_validate($form, &$form_state) {
  if ($form_state['values']['merchant_id'] == '' or
    $form_state['values']['merchant_id'] == 'Please supply a Merchant ID') {
    form_set_error('', t('You must supply a valid Merchant ID.'));
  }

  if ($form_state['values']['public_key'] == '' or
    $form_state['values']['public_key'] == 'Please supply a Public Key') {
    form_set_error('', t('You must supply a valid Public Key.'));
  }

}

/**
 * Function used load form to set Braintree Configuration variables.
 */
function uc_braintree_tr_payment_payment_settings_form_submit($form, &$form_state) {
  foreach ($form_state['values'] as $key => $value) {
    variable_set('uc_braintree_tr_payment_' . $key, $value);
  }
}

/**
 * Function used to retrieve a complete listing of Braintree subscription data.
 */
function uc_braintree_tr_payment_user_subscriptions() {

  $rows = array();
  $output = '';

  $header = array(t('E-mail'),t('Plan'),t('Subscription'),t('Price'),
    t('Next Bill Date'),t('Credit Card'),t('Payment History'),
    t('Drupal Status'),t('Braintree Status'));

  try {
    $plans = uc_braintree_tr_payment_get_subscription_plans();
  }
  catch (Exception $e){
    uc_braintree_tr_payment_connection_exception($e);
  }

  $output = "<br /><br />" . theme('table', $header, $rows);

  $subscriptions = uc_braintree_tr_payment_get_user_subscriptions("");

  $results = uc_braintree_tr_payment_get_braintree_customer_token_subscription();
  $braintree_subscriptions = $results['3'];

  if (count($subscriptions) > 0) {
    foreach ($subscriptions as $subscription_id => $value) {
      $rows[] = array(
        $value['email'],
        $plans[$braintree_subscriptions[$subscription_id]['planId']]['name'],
        $subscription_id,
        $braintree_subscriptions[$subscription_id]['price'],
        $braintree_subscriptions[$subscription_id]['nextBillingDate'],
        $value['label'],
        l(t('Detail'), 'user/' . $value['uid'] . '/user-subscription/' .
        $subscription_id . '/detail/'),
        $value['status'],
        $braintree_subscriptions[$subscription_id]['status']);
    }
  }

  if (!empty($rows)) {
    $output = "<br /><br />" . theme('table', $header, $rows);
  }
  else {
    $output = "<br /><br />No subscription could be found.";
  }

  return $output;
}

/**
 * Function used to display or load a Braintree report.
 */
function uc_braintree_tr_payment_admin_store_braintree_reports($report) {

  switch ($report) {
    case 'subscriptions':
      $output = uc_braintree_tr_payment_user_subscriptions();
      break;

    default:
      $output = "<br /><br />" .
        l(t('View All User Subscription Information'),
          'admin/store/braintree/reports/subscriptions');
  }

  return $output;

}

/**
 * Confirmation form for Braintree tool functions.
 */
function uc_braintree_tr_payment_admin_store_braintree_tools_confirm_form($form_state, $tool = NULL) {
  uc_braintree_tr_payment_configuration_initialization();

  $form = array();
  $form['#attributes'] = array('name' => 'uc_braintree_tr_payment_admin_store_braintree_tools_confirm');

  $form['confirm'] = array(
    '#type' => 'fieldset',
    '#title' => t('Confirm'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );

  $form['confirm']['tool'] = array(
    '#type' => 'hidden',
    '#name' => "tool",
    '#value' => $tool,
  );

  if ($tool == "populate_sandbox") {
    $form['confirm']['message'] = array(
      '#type' => 'markup',
      '#value' => "<p>Running this tool will create sample users and tokens " .
      " in your Braintree sandbox account for testing. \n" .
      "Are you sure you want to perform this operation? " .
      "Click 'Yes' to continue.  Click 'No' to cancel.</p>",
    );
  }
  elseif ($tool == "rebuild") {
    $form['confirm']['message'] = array(
      '#type' => 'markup',
      '#value' => "<p>Running this tool will cause the uc_braintree_tr_payment " .
      "related database tables to be updated to align with user token and " .
      "subscription information contained within the Braintree API. The " .
      "feature is designed to be used in the event of a disaster. Anything " .
      "which was deleted or irreparably removed will be restored, provided a " .
      "Drupal user account exists using the same e-mail address from the time " .
      "of purchase. Please be aware that if a user e-mail address is listed " .
      "more than one time in the Braintree vault for the same website, the " .
      "token and subscription information will be consolidated to a single " .
      "Drupal user corresponding with the e-mail address. This duplication " .
      "sometimes occurs when a new customer ID value is created each time an " .
      "authenticated user performs a transaction with a new credit card. " .
      "This rebuild function may be used on existing table information, but " .
      "it is advisable that you first backup the uc_braintree_tr_payment " .
      "tables prior to execution. Are you sure you want to perform this " .
      "operation? Click 'Yes' to continue. Click 'No' to cancel.</p>",
    );
  }

  $form['confirm']['yes'] = array(
    '#name' => 'yes',
    '#type' => 'submit',
    '#value' => t('Yes'),
  );

  $form['confirm']['no'] = array(
    '#name' => 'no',
    '#type' => 'submit',
    '#value' => t('No'),
  );

  return $form;
}

/**
 * Page callback for transaction completion.
 */
function uc_braintree_tr_payment_admin_store_braintree_tools_confirm_form_submit() {
  uc_braintree_tr_payment_configuration_initialization();

  if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $path_parts = pathinfo($_SERVER['REQUEST_URI']);

    if (isset($_REQUEST['yes'])) {
      if (isset($_REQUEST['tool']) && $_REQUEST['tool'] == "populate_sandbox") {
        $output = "https://" . $_SERVER['HTTP_HOST']  . $path_parts['dirname'] .
          "/populate_sandbox_confirmed";

      }
      elseif (isset($_REQUEST['tool']) && $_REQUEST['tool'] == "rebuild") {
        $output = "https://" . $_SERVER['HTTP_HOST']  . $path_parts['dirname'] . "/rebuild_confirmed";

      }
    }
    else {
      $output = "https://" . $_SERVER['HTTP_HOST']  . $path_parts['dirname'];

    }

    drupal_goto($output);
  }
}

/**
 * Function used to call a utility listed under the Braintree Tools task menu.
 */
function uc_braintree_tr_payment_admin_store_braintree_tools($tool) {

  switch ($tool) {

    case 'rebuild':
      $output = "<div>" .
            drupal_get_form('uc_braintree_tr_payment_admin_store_braintree_tools_confirm_form', $tool) . "</div>";
      break;

    case 'populate_sandbox':
      $output = "<div>" .
            drupal_get_form('uc_braintree_tr_payment_admin_store_braintree_tools_confirm_form', $tool) . "</div>";
      break;

    case 'populate_sandbox_confirmed':
      require_once 'uc_braintree_tr_payment_populate_sandbox.inc';
      $create_count = uc_braintree_tr_payment_populate_sandbox();
      $output = "$create_count sample users and tokens have been added " .
        " to the sandbox for testing.";
      break;

    case 'rebuild_confirmed':
      require_once 'uc_braintree_tr_payment_rebuild.inc';
      $sku_role_id = uc_braintree_tr_payment_get_sku_role_id();
      uc_braintree_tr_payment_rebuild_tables($sku_role_id);
      $output = "Local Braintree table information has been rebuilt. " .
        "To view all user subscriptions, click " .
        l(t('here'), 'admin/store/braintree/reports/subscriptions');
      break;

    default:
      $output = "<br /><br />" .
        l(t('Rebuild Local Braintree Table Information'),
          'admin/store/braintree/tools/rebuild') .
        "<br /><br />";
      if (variable_get('uc_braintree_tr_payment_environment', 'sandbox') ==
        'sandbox') {
        $output .= l(t('Generate Sample Drupal Users and Sandbox Tokens'),
          'admin/store/braintree/tools/populate_sandbox');
      }

  }

  return $output;
}
