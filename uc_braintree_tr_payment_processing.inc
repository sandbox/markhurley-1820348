<?php
/**
 * @file
 * Include file containing functions used to alter or construct the forms
 * for the Braintree transparent redirection and server-to-server processing,
 * and provide the transaction confirmation via the Braintree Gateway.
 */

/**
 * Payment method update form and processing.
 */
function uc_braintree_tr_payment_update_payment_method($user, $operation, $subscription_id = NULL) {

  $token = $_REQUEST['token'];
  $default_token = $_REQUEST['defaultToken'];

  $customer_id = uc_braintree_tr_payment_get_customer_id($user->uid);

  if ((!($operation)) and $_REQUEST['paymentAction']) {
    $operation = $_REQUEST['paymentAction'];
  }

  if ((!($subscription_id)) and $_REQUEST['subscriptionId']) {
    $subscription_id = $_REQUEST['subscriptionId'];
  }

  switch ($operation) {

    case 'addCard':
      $query_string = preg_replace('/^q=&(.*)$/','$1',$_SERVER['QUERY_STRING']);

      try {
        uc_braintree_tr_payment_configuration_initialization();

        try {
          $result = Braintree_TransparentRedirect::confirm($query_string);
        }
        catch (Exception $e) {
          uc_braintree_tr_payment_log_error_message("Exception: $e");
          return "The payment method could not be added at this time. Please try again later.";
        }

        if ($result->success) {

          $token = $result->creditCard->token;

          $credit_card = uc_braintree_tr_payment_get_last_four($token);

          uc_braintree_tr_payment_save_token($user->uid, $token, $credit_card['0'], $credit_card['1']);

          $output = "<div><h3>Payment Method Added</h3><br /><br />" .
              "Your new card ending in <b>" . $credit_card['1'] . "</b> has been successfully added.<br /><br />";

          if ($subscription_id) {

            uc_braintree_tr_payment_save_customer_subscription_token($user->uid, $token, $subscription_id);
            $output .= "Recurring payment $subscription_id has been updated to use the new credit card information.<br /><br />";
          }

          if ($customer_id && $user == user_uid_optional_load()) {
            $output .= l(t('Add Credit Card'), 'user/' . $user->uid . '/payment-method/addCardForm/') . "<br />";
          }

          $output .= uc_braintree_tr_payment_token_table($user->uid);

        }
        else {
          require_once 'uc_braintree_tr_payment_redirect_error_handling.inc';
          $output = "<div><h3>Payment Method Submission</h3><br />";
          $output .= uc_braintree_tr_payment_redirect_error_handling($result);
        }
      }
      catch (Exception $e) {
        uc_braintree_tr_payment_connection_exception($e);
      }
      break;

    case 'removeCard':
      try {
        $credit_card = uc_braintree_tr_payment_get_last_four($token);

        if (uc_braintree_tr_payment_remove_token($user->uid, $token)) {
          $output = "<div><h3>Payment Method Removed</h3><br /><br />" .
              "Your card ending in <b>" . $credit_card['1'] . "</b> has been successfully removed FROM your payment methods.<br /><br />";
        }
        else {
          $output = "<div>The card could not be removed at this time. &nbsp; Please contact a system administrator.<br /><br />";
        }

        if ($customer_id && $user == user_uid_optional_load()) {
          $output .= l(t('Add Credit Card'), 'user/' . $user->uid . '/payment-method/addCardForm/') . "<br />";
        }

        $output .= uc_braintree_tr_payment_token_table($user->uid);
      }
      catch (Exception $e) {
        uc_braintree_tr_payment_connection_exception($e);
      }
      break;

    case 'addCardForm':

      if ($customer_id && $user == user_uid_optional_load()) {
        $output = "<div><h4>Add Credit Card</h4>" .
            drupal_get_form('uc_braintree_tr_payment_create_card_form', $subscription_id) . "</div>";
      }
      break;

    case 'selectPaymentMethod':
    	$subscriptions = uc_braintree_tr_payment_get_subscription_status($subscription_id);
    	$plans = uc_braintree_tr_payment_get_subscription_plans();

    	$output = "<div><h4>" . $plans[$subscriptions[$subscription_id]['planId']]['name'] .
      		" - Select Payment Method</h4>" .
      		l(t('Manage Payment Methods'), 'user/' . $user->uid . '/payment-method/') .
      		drupal_get_form('uc_braintree_tr_payment_token_selection_form', $subscription_id, $default_token, $user->uid);

	    if ($customer_id && $user == user_uid_optional_load()) {
	      $output .=
	        "<h4>Add Credit Card</h4>" .
	        drupal_get_form('uc_braintree_tr_payment_create_card_form', $subscription_id);
	    }
	    $output .= "</div>";

      break;

    case 'paymentMethodSelection':
      $token = check_plain($_POST['token']);

      try {

        uc_braintree_tr_payment_save_customer_subscription($user->uid, $token, '0', $subscription_id, '0', '1', '0');

        $credit_card = uc_braintree_tr_payment_get_last_four($token);

        $plans = uc_braintree_tr_payment_get_subscription_plans();

        $subscription = Braintree_Subscription::find($subscription_id);

        $output = "<div><h3>Payment Method Updated</h3><br /><br />" .
            "Recurring payment " . $subscription_id .
            "&nbsp; <b><i>" .  $plans[$subscription->planId]['name']  . "</i></b>" .
            " will now be processed with " . $credit_card['0'] . " card ending in <b>" . $credit_card['1'] . "</b>.";

        $output .= "<br /><br />" . l(t('Back to All Recurring Payments'), 'user/' . $user->uid . '/user-subscription/');
      }
      catch (Exception $e) {
        uc_braintree_tr_payment_connection_exception($e);
      }
      break;

    default:
      if ($customer_id && $user == user_uid_optional_load()) {
        $output = l(t('Add Credit Card'), 'user/' . $user->uid . '/payment-method/addCardForm/') . "<br />";
      }
      else {
        $output = "";
      }

      $output .= uc_braintree_tr_payment_token_table($user->uid);

  }

  return $output;
}

/**
 * Page callback for transaction completion.
 */
function uc_braintree_tr_payment_complete() {

  $payment_action = $_REQUEST['paymentAction'];

  if (!(uc_braintree_tr_payment_configuration_initialization())) {
    return;
  }

  switch ($payment_action) {

    case 'transparentRedirect':

      $query_string = preg_replace('/^q=&(.*)$/','$1',$_SERVER['QUERY_STRING']);

      $order_id = $_REQUEST['orderId'];

      $order = uc_order_load($order_id);

      try {
        $result = Braintree_TransparentRedirect::confirm($query_string);

        if (variable_get('uc_braintree_tr_payment_transaction_debugging', NULL)) {
          uc_braintree_tr_payment_redirect_transaction_debugging($result);
        }

        if ($result->success and $result->transaction->processorResponseCode <= 2000) {

          $token = $result->transaction->creditCardDetails->token;
          $card_type = $result->transaction->creditCardDetails->cardType;
          $last_four = $result->transaction->creditCardDetails->last4;

          $comment = "Card charged, transaction code: " . $result->transaction->id;

          $sale = uc_cart_complete_sale($order, variable_get('uc_new_customer_login', TRUE));

          uc_payment_enter($order->order_id, 'token', $order->order_total, $order->uid, NULL, $comment);

          uc_braintree_tr_payment_save_token($order->uid, $token, $card_type, $last_four);

          uc_braintree_tr_payment_log_debug_message(t("Braintree Customer ID: %customer_id",
              array('%customer_id' => $result->transaction->customerDetails->id)), "status", FALSE);

          $customer_id = uc_braintree_tr_payment_get_customer_id($order->uid);

          uc_braintree_tr_payment_log_debug_message(t("Stored database Customer ID: %customer_id",
              array('%customer_id' => $customer_id)));

          try {
            uc_braintree_tr_payment_save_customer($order->uid, $result->transaction->customerDetails->id);
          }
          catch (Exception $e) {
            uc_braintree_tr_payment_log_debug_message(t("Error saving customer information: %exception",
                array('%exception' => $e)));
          }

          $product_subscriptions = uc_braintree_tr_payment_identify_product_subscriptions($order);

          uc_braintree_tr_payment_submit_product_subscriptions($order->uid, $token, $order_id, $product_subscriptions, $result->transaction->id);

          if ($sku_map = variable_get('skuMap', NULL)) {
            require_once 'uc_braintree_tr_payment_cancel_payflow_pro.inc';
            uc_braintree_tr_payment_cancel_payflowpro_model($order, $sku_map);
          }

          $page = variable_get('uc_cart_checkout_complete_page', '');

          if (!empty($page)) {
            drupal_goto($page);
          }

          return $sale;

        }
        else {
          require_once 'uc_braintree_tr_payment_redirect_error_handling.inc';
          $output = uc_braintree_tr_payment_redirect_error_handling($result);
        }

      }
      catch (Exception $e) {
        uc_braintree_tr_payment_connection_exception($e);
      }
      break;

    case 'server2server':
      $order_id = $_SESSION['cart_order'];

      $order = uc_order_load($order_id);

      $skuValues = array();
      foreach ($order->products as $key) {
        $skuValues[] = $key->model;
        for($i = $key->qty; $i > 1; $i--) {
          $skuValues[] = $key->model;
        }
      }
      $skuValue = implode("|", $skuValues);
      if(strlen($skuValue) > 254) {
        $skuValue = substr($skuValue, 0, 254);
      }

      $result = db_query("SELECT message FROM {uc_order_comments} WHERE order_id = %d", $order->order_id);
      if ($row = db_fetch_object($result)) {
        $comments = $row->message;
      }

      $token = check_plain($_POST['token']);

      try {
        $result = Braintree_CreditCard::sale(
            $token,
            array(
              'amount' => $order->order_total,
              'orderId' => $order->order_id,
              'deviceData' => $_REQUEST["device_data"],
              'options' => array(
                'storeInVault' => 'TRUE',
                'submitForSettlement' => 'TRUE',
              ),
              'customer' => array(
                'firstName' => $order->billing_first_name,
                'lastName' => $order->billing_last_name,
                'company' => $order->billing_company,
                'phone' => $order->billing_phone,
              ),
              'billing' => array(
                'firstName' => $order->billing_first_name,
                'lastName' => $order->billing_last_name,
                'company' => $order->billing_company,
                'streetAddress' => $order->billing_street1,
                'extendedAddress' => $order->billing_street2,
                'locality' => $order->billing_city,
                'region' => $order->billing_zone,
                'postalCode' => $order->billing_postal_code,
                'countryCodeNumeric' => $order->billing_country
              ),
              'customFields' => array(
                'sku' => $skuValue,
                'comments' => $comments,
              ),
            	'channel' => 'PioneeringSoftware_SP_Braintree',
            )
        );

        uc_braintree_tr_payment_redirect_transaction_debugging($result);

        if ($result->success and $result->transaction->processorResponseCode <= 2000) {

          $comment = "Card charged, transaction code: " . $result->transaction->id;

          $sale = uc_cart_complete_sale($order, FALSE);

          uc_payment_enter($order->order_id, 'token', $order->order_total, $order->uid, NULL, $comment);

          $product_subscriptions = uc_braintree_tr_payment_identify_product_subscriptions($order);

          uc_braintree_tr_payment_submit_product_subscriptions($order->uid, $token, $order_id, $product_subscriptions, $result->transaction->id);

          if ($sku_map = variable_get('skuMap', NULL)) {
            require_once 'uc_braintree_tr_payment_cancel_payflow_pro.inc';
            uc_braintree_tr_payment_cancel_payflowpro_model($order, $sku_map);
          }

          $page = variable_get('uc_cart_checkout_complete_page', '');

          if (!empty($page)) {
            drupal_goto($page);
          }

          return $sale;
        }
        else {
          require_once 'uc_braintree_tr_payment_redirect_error_handling.inc';
          $output = uc_braintree_tr_payment_redirect_error_handling($result);
        }
      }
      catch (Exception $e) {
        uc_braintree_tr_payment_connection_exception($e);
      }
      break;
  }

  return $output;
}
