<?php
/**
 * @file
 * Include file containing functions used to manage user subscriptions.
 */

/**
 * Method used to load the subscription user fees theme.
 */
function uc_braintree_tr_payment_user_subscription($user, $subscription_id = NULL, $operation = NULL, $param1 = NULL, $param2 = NULL) {

  $account = user_load($user->uid);

  switch ($operation) {
    case 'cancelX':
      if (uc_braintree_tr_payment_cancel_user_subscription_access($account)) {
        $plan_name = $param1;
        return t("<h4>Confirm Cancelation</h4>Are you sure you would like to cancel the recurring payment <i><b>%plan_name</b></i>?<br/><br/>",
            array('%plan_name' => $plan_name)) .
            drupal_get_form('uc_braintree_tr_payment_user_subscription_cancel_confirm', $user->uid, $subscription_id);
      }
      else {
        drupal_access_denied();
        break;
      }

    case 'cancel':
      if (uc_braintree_tr_payment_cancel_user_subscription_access($account)) {
        uc_braintree_tr_payment_cancel_subscription($user->uid, $subscription_id, 1);
        return theme('uc_braintree_tr_payment_user_subscription_table', $user->uid);
      }
      else {
        drupal_access_denied();
        break;
      }

    case 'changeBillingDateForm':
      if (uc_braintree_tr_payment_change_bill_date_subscription_access($account)) {
        $token = $param1;
        $plan_name = $param2;
        return "<h4>Change Billing Day of Month</h4>" .
            "Please SELECT the day of month for the desired billing of " .
            "recurring payment <i><b>" . $plan_name . "</b></i><br/><br/>" .
            drupal_get_form('uc_braintree_tr_payment_change_billing_date_form',
                $user->uid, $subscription_id, $token);
      }
      else {
        drupal_access_denied();
        break;
      }

    case 'changeBillingDateX':
      if (uc_braintree_tr_payment_change_bill_date_subscription_access($account)) {
        $token = $param1;
        $plan_name = $param2;
        return "<h4>Confirm Billing Date Change Transaction Amount</h4>" .
            uc_braintree_tr_payment_change_billing_date_trial_calculation($subscription_id) .
            "<br/><br/>" .
            drupal_get_form('uc_braintree_tr_payment_change_billing_date_form_confirm',
                $user->uid, $subscription_id, $token);
      }
      else {
        drupal_access_denied();
        break;
      }

    case 'changeBillingDate':
      if (uc_braintree_tr_payment_change_bill_date_subscription_access($account)) {
        $token = $param1;
        uc_braintree_tr_payment_replace_subscription($user->uid, $subscription_id, $token);
        return theme('uc_braintree_tr_payment_user_subscription_table', $user->uid);
      }
      else {
        drupal_access_denied();
        break;
      }

    case 'detail':
      if (uc_braintree_tr_payment_cancel_user_subscription_access($account)) {
        return theme('uc_braintree_tr_payment_user_subscription_detail_table', $user->uid, $subscription_id);
      }
      else {
        drupal_access_denied();
        break;
      }

    default:
      if (uc_braintree_tr_payment_cancel_user_subscription_access($account)) {
        return theme('uc_braintree_tr_payment_user_subscription_table', $user->uid);
      }
      else {
        drupal_access_denied();
        break;
      }
  }

}

/**
 * Function used to confirm the cancelation of a user subscription.
 */
function uc_braintree_tr_payment_user_subscription_cancel_confirm($form, $uid, $subscription_id) {

  $base_url = base_path() . '?q=user/' . $uid . '/user-subscription/' .
      $subscription_id . '/cancel/';

  $form['#action'] = $base_url;
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Confirm'),
  );
  $form['noCancel'] = array(
    '#name' => 'noCancel',
    '#type' => 'submit',
    '#value' => t('Do Not Cancel'),
  );
  return $form;
}

/**
 * Function used to cancel a user subscription.
 */
function uc_braintree_tr_payment_cancel_subscription($uid, $subscription_id, $revoke_role) {
  // Cancelation includes the revocation of a user role assigned with the
  // product subscription.
  if ($_REQUEST["noCancel"]) {
    return;
  }

  if (!(uc_braintree_tr_payment_configuration_initialization())) {
    return;
  }

  $sku_role_id = uc_braintree_tr_payment_get_sku_role_id();

  try {
    Braintree_Subscription::cancel($subscription_id);
  }
  catch (Exception $e) {
    uc_braintree_tr_payment_connection_exception($e);
  }

  if ($revoke_role) {
    $subscriptions = uc_braintree_tr_payment_get_subscription_status($subscription_id);
    $account = user_load($uid);

    if (count($subscriptions) > 0) {
      foreach ($subscriptions as $subscription_id => $s_value) {
        if (count($sku_role_id[$s_value['planId']]) > 0) {
          foreach ($sku_role_id[$s_value['planId']] as $role_id => $r_value) {
            if (uc_braintree_tr_payment_is_unique_role($s_value['uid'], $role_id)) {
              uc_roles_revoke($account, $role_id, TRUE);
              uc_braintree_tr_payment_log_debug_message(t("Role revoked for user : %uid Role: %role_id",
                  array('%uid' => $s_value['uid'], '%role_id' => $role_id)));
              uc_braintree_tr_payment_log_debug_message(t("Recurring payment %subscription_id canceled.",
                  array('%subscription_id', $subscription_id)));
            }
          }
        }
      }
    }
  }

  db_query("UPDATE {uc_braintree_tr_payment_customer_subscription} set status = 'Canceled', last_modified = UNIX_TIMESTAMP(now())
      where uid = '%d' and subscription_id = '%s'", $uid, $subscription_id);

}

/**
 * Theme to load to the table containing all of a user's tokens.
 */
function theme_uc_braintree_tr_payment_user_subscription_table($uid) {

  uc_braintree_tr_payment_setup_timezone();

  $rows = array();
  $output = '';

  $header = array(t('ID'), t('Plan'), t('Price'), t('Next Bill Date'),
    t('Credit Card'), t('Manage Payment Methods'), t('Payment History'),
    t('Status'), t('Cancel'));

  // Make sure the DateTime constructor is working properly before offering
  // the Change Billing Date option to the user.
  $allow_billing_date_change = 1;
  try {
    // Test the DateTime construct to insure a timezone value is configured
    // for billing date changes; Will product an exception if the timezone
    // has not been configured properly.
    $sample = new DateTime("now");
    $sample->setDate($sample->format('Y'), $sample->format('m'), 15);
    $sample->setTime(0, 0, 0);
  }
  catch (Exception $e) {
    $allow_billing_date_change = 0;
  }

  try {
    try {
      $plans = uc_braintree_tr_payment_get_subscription_plans();
    }
    catch (Exception $e) {
      uc_braintree_tr_payment_log_error_message("Problem getting subscription plans: $e");
    }

    try {
      $subscriptions = uc_braintree_tr_payment_get_user_subscriptions($uid);
    }
    catch (Exception $e) {
      uc_braintree_tr_payment_log_error_message("Problem getting user subscriptions: $e");
    }

    if (count($subscriptions) > 0) {

      if (!(uc_braintree_tr_payment_configuration_initialization())) {
        return;
      }

      foreach ($subscriptions as $subscription_id => $value) {

        try {
          if ($subscription_id) {
            $subscription = Braintree_Subscription::find($subscription_id);
          }
          // Do not present the user with the "Change Date" option unless
          // the subscription has fulfilled one billing period, as validated
          // by a transaction id retreived from
          // $subscription->_attributes[transactions][0]->id
          // and the $sample DateTime construction was a success as identified
          // by $allow_billing_date_change.
          $rows[] = array(
            array(
              'data' => $subscription->id,
              'align' => 'center',
              'width' => '75',
            ),
            array(
              'data' => $plans[$subscription->planId]['name'],
              'align' => 'center',
            ),
            array('data' => $subscription->price, 'align' => 'right'),
            array(
              'data' => $subscription->nextBillingDate->format('Y-m-d') .
              '&nbsp;' . (($subscription->_attributes['transactions']['0']->id and $subscription->_attributes['transactions']['0']->status == 'settled'
                  and $value['status'] <> 'Canceled' and $allow_billing_date_change == 1) ? l(t('Change Date'), 'user/' . $uid . '/user-subscription/' . $subscription->id  . '/changeBillingDateForm/' .
                      $value['token'] . '/' . check_plain($plans[$subscription->planId]['name'])) : "&nbsp;"),
              'align' => 'center',
            ),
            array('data' => $value['label'], 'align' => 'center'),
            array(
              'data' => ($value['status'] <> 'Canceled' ? l(t('Update'), 'user/' .
                $uid . '/payment-method/selectPaymentMethod/' . $subscription_id . '/',
                array(
                  'html' => TRUE,
                  'query' => array(
                    'defaultToken' => $value['token'],
                  ),
                )
              ) : 'n/a'),
              'align' => 'center',
            ),
            array(
              'data' => l(t('Detail'), 'user/' . $uid . '/user-subscription/' . $subscription_id  . '/detail/'),
              'align' => 'center',
            ),
            array(
              'data' => ($value['status'] ? $value['status'] : 'Pending'),
              'align' => 'center',
            ),
            array(
              'data' => ($value['status'] <> 'Canceled' ? l(t('Cancel'), 'user/' . $uid . '/user-subscription/' . $subscription_id  . '/cancelX/' .
                  check_plain($plans[$subscription->planId]['name'])) : 'n/a'),
              'align' => 'center',
            ),
          );
        }
        catch (Exception $e) {
          if (variable_get('uc_braintree_tr_payment_transaction_debugging', NULL)) {
            uc_braintree_tr_payment_log_error_message("Problem finding a subscription ID");
          }
          uc_braintree_tr_payment_connection_exception($e);
        }
      }
    }

  }
  catch (Exception $e) {
    uc_braintree_tr_payment_connection_exception($e);
  }

  if (!empty($rows)) {
    $output = "<br /><br />" . theme('table', $header, $rows);
  }
  else {
    $output = "<br /><br />No recurring bills could be found.";
  }

  return $output;
}

/**
 * Theme used to display the user subscription billing history.
 */
function theme_uc_braintree_tr_payment_user_subscription_detail_table($uid, $subscription_id) {

  if (!(uc_braintree_tr_payment_configuration_initialization())) {
    return;
  }

  try {
    $plans = uc_braintree_tr_payment_get_subscription_plans();

    $subscription = Braintree_Subscription::find($subscription_id);

    $output = "<h3>Recurring Payment History</h3>" .
        "Recurring Payment: <b><i>$subscription_id &nbsp; " .
        $plans[$subscription->planId]['name'] . "</i></b><br /><br />";

    $header = array(t('Recurring Payment'), t('Date'), t('Payment Method'),
      t('Amount'), t('Type'), t('Status'));

    $rows = array();

    foreach ($subscription->_attributes['transactions'] as $value) {
      $rows[] = array(
        array('data' => $subscription_id, 'align' => 'center'),
        array(
          'data' => $value->createdAt->format('Y-m-d'),
          'align' => 'center',
        ),
        array(
          'data' => $value->creditCardDetails->cardType .
          " Ending in " . $value->creditCardDetails->last4,
          'align' => 'center',
        ),
        array('data' => $value->amount, 'align' => 'right'),
        array('data' => $value->type, 'align' => 'center'),
        array('data' => $value->status, 'align' => 'left'),
      );
    }

    if (!empty($rows)) {
      $output .= theme('table', $header, $rows);
    }
    else {
      $output .= "<br />No billing history information is presently available for recurring payment <b>$subscription_id</b>.";
    }

  }
  catch (Exception $e) {
    uc_braintree_tr_payment_connection_exception($e);
  }

  $rows = array();

  $old_subscription = uc_braintree_tr_payment_get_old_subscription($uid, $subscription_id);

  $old_subscription_id = $old_subscription['0'];
  $date_replaced = $old_subscription['1'];

  while ($old_subscription_id) {

    $output .= "<br /><br />Recurring payment <b>$old_subscription_id</b> was replaced by recurring payment <b>$subscription_id</b> on $date_replaced.<br /><br />";

    try {
      $subscription = Braintree_Subscription::find($old_subscription_id);
      foreach ($subscription->_attributes['transactions'] as $value) {
        $rows[] = array(
          array('data' => $subscription_id, 'align' => 'center'),
          array(
            'data' => $value->createdAt->format('Y-m-d'),
            'align' => 'center',
          ),
          array(
            'data' => $value->creditCardDetails->cardType .
            " Ending in " . $value->creditCardDetails->last4,
            'align' => 'center',
          ),
          array('data' => $value->amount, 'align' => 'right'),
          array('data' => $value->type, 'align' => 'center'),
          array('data' => $value->status, 'align' => 'left'),
        );
      }
    }
    catch (Exception $e) {
      uc_braintree_tr_payment_connection_exception($e);
    }

    $subscription_id = $old_subscription_id;

    $old_subscription = uc_braintree_tr_payment_get_old_subscription($uid, $old_subscription_id);
    $old_subscription_id = $old_subscription['0'];
    $date_replaced = $old_subscription['1'];
  }

  if (!empty($rows)) {
    $output .= theme('table', $header, $rows);
  }
  elseif ($old_subscription_id) {
    $output .= "<br />No billing history information is presently available for recurring payment <b>$old_subscription_id</b>.";
  }

  $output .= "<br /><br />" . l(t('Back to All Recurring Payments'), 'user/' . $uid . '/user-subscription/');

  return $output;
}

/**
 * Function which builds a form for a user to change the next billing date.
 */
function uc_braintree_tr_payment_change_billing_date_form($form, $uid, $subscription_id, $token) {
  $form = array();

  $method_action = "changeBillingDateX";

  for ($i = 1; $i < 29; $i++) {
    $days[] = $i;
  }

  $form['billingDayOfMonth'] = array(
    '#title' => t('Billing Day of Month'),
    '#type' => 'select',
    '#options' => drupal_map_assoc($days),
    '#default_value' => array(
      'day' => format_date(time(), 'custom', 'j'),
    ),
  );

  $base_url = base_path() . '?q=user/' . $uid . '/user-subscription/' . $subscription_id . '/' . $method_action . '/' . $token;

  $form['#action'] = $base_url;

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );
  $form['noCancel'] = array(
    '#name' => 'noBillingDateChange',
    '#type' => 'submit',
    '#value' => t('Cancel'),
  );

  return $form;
}

/**
 * Function which builds a form confirm the change for the next billing date.
 */
function uc_braintree_tr_payment_change_billing_date_form_confirm($form, $uid, $subscription_id, $token) {
  $form = array();

  $method_action = "changeBillingDate";
  $form['billingDayOfMonth'] = array(
    '#type' => 'hidden',
    '#value' => $_REQUEST['billingDayOfMonth'],
  );

  $base_url = base_path() . '?q=user/' . $uid . '/user-subscription/' . $subscription_id . '/' . $method_action . '/' . $token;

  $form['#action'] = $base_url;

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );
  $form['noCancel'] = array(
    '#name' => 'noBillingDateChange',
    '#type' => 'submit',
    '#value' => t('Cancel'),
  );

  return $form;
}

/**
 * Function used to calculate the transaction credit/refund.
 */
function uc_braintree_tr_payment_change_billing_date_trial_calculation($subscription_id) {

  $transaction_type = "";
  $amount = "";

  uc_braintree_tr_payment_setup_timezone();

  if ($_REQUEST["noBillingDateChange"]) {
    return;
  }

  if (!(uc_braintree_tr_payment_configuration_initialization())) {
    return;
  }

  try {
    $subscription = Braintree_Subscription::find($subscription_id);

    if ($subscription->_attributes['transactions']['0']->status <> 'settled') {
      uc_braintree_tr_payment_log_debug_message(t("The request may not be processed at this time.  &nbsp;  The recurring payment is awaiting settlement and may not be credited at this time. &nbsp; Please contact an administrator if you believe you have received this message in error."));
      return;
    }
  }
  catch (Exception $e) {
    uc_braintree_tr_payment_connection_exception($e);
  }

  $billing_day_of_month = $_REQUEST['billingDayOfMonth'];

  uc_braintree_tr_payment_log_debug_message(t("day of month: %billing_day_of_month",
      array('%billing_day_of_month' => $billing_day_of_month)));

  $now = new DateTime("now");

  $nbd = new DateTime("now");
  $nbd->setDate($nbd->format('Y'), $nbd->format('m'), $billing_day_of_month);
  $nbd->setTime(0, 0, 0);

  if ($nbd > $now) {
    uc_braintree_tr_payment_log_debug_message(t("%next_billing_date occurs in future",
        array('%next_billing_date' => $nbd->format('Y-m-d'))));
  }
  else {
    uc_braintree_tr_payment_log_debug_message(t("%next_billing_date occurs in past",
        array('%next_billing_date' => $nbd->format('Y-m-d'))));

    if ($nbd->format('m') + 1 > 12) {
      $month = 1;
      $year = $nbd->format('Y') + 1;
    }
    else {
      $month = $nbd->format('m') + 1;
      $year = $nbd->format('Y');
    }
    $nbd->setDate($year, $month, $nbd->format('d'));

  }

  $obd = new DateTime("now");
  $obd->setDate($subscription->nextBillingDate->format('Y'), $subscription->nextBillingDate->format('m'), $subscription->nextBillingDate->format('d'));
  $obd->setTime(0, 0, 0);

  uc_braintree_tr_payment_log_debug_message(t("payment method token: %token",
      array('%token' => $subscription->paymentMethodToken)));
  uc_braintree_tr_payment_log_debug_message(t("planId: %plan_id",
      array('%plan_id' => $subscription->planId)));
  uc_braintree_tr_payment_log_debug_message(t("price: %price",
      array('%price' => $subscription->price)));
  uc_braintree_tr_payment_log_debug_message(t("Old Billing Date: %old_billing_date",
      array('%old_billing_date' => $obd->format('Y-m-d'))));
  uc_braintree_tr_payment_log_debug_message(t("Next Billing Date: %next_billing_date",
      array('%next_billing_date', $nbd->format('Y-m-d'))));

  $daily_price = $subscription->price / 30.4375;

  uc_braintree_tr_payment_log_debug_message(t("Daily Price: %daily_price",
      array('%daily_price', $daily_price)));

  $credit_days = (int) round(($now->format('U') - $obd->format('U')) / (60 * 60 * 24));

  uc_braintree_tr_payment_log_debug_message(t("Credit Days: %credit_days",
      array('%credit_days' => $credit_days)));

  $charge_days = (int) round(($nbd->format('U') - $now->format('U')) / (60 * 60 * 24));

  uc_braintree_tr_payment_log_debug_message(t("Charge Days: %charge_days",
      array('%charge_days' => $charge_days)));

  $adjustment_in_days = (int) round($credit_days + $charge_days);

  if ($adjustment_in_days < 0) {
    if ($subscription->_attributes['transactions']['0']->id) {
      $transaction_type = "refund";
      $amount = sprintf("%.2f", $adjustment_in_days * $daily_price * -1);

      uc_braintree_tr_payment_log_debug_message(t("Refund: %refund",
          array('%refund' => $amount)));
    }
    else {
      uc_braintree_tr_payment_log_debug_message(t("A transaction id does not yet exist for this recurring payment"));
    }

  }
  elseif ($adjustment_in_days > 0) {
    $transaction_type = "charge";
    $amount = sprintf("%.2f", $adjustment_in_days * $daily_price);

    uc_braintree_tr_payment_log_debug_message(t("Transaction: %amount",
        array('%ammount' => $amount)));
  }

  $output = t("The request to change the next billing date from %old_billing_date to %new_billing_date will result in a prorated %type of %amount. &nbsp;  All subsequent billings will occur on day %billing_day_of_month of each month. &nbsp; To accept this change in billing date and perform this prorated transaction, please click <i>Submit</i> or else click <i>Cancel</i>",
      array(
        '%old_billing_date' => $obd->format('Y-m-d'),
        '%new_billing_date' => $nbd->format('Y-m-d'),
        '%type' => $transaction_type,
        '%amount' => uc_currency_format($amount),
        '%billing_day_of_month' => $billing_day_of_month,
      )
  );

  return $output;

}

/**
 * Function used to perform a prorated transaction based.
 */
function uc_braintree_tr_payment_replace_subscription($uid, $subscription_id, $token) {

  uc_braintree_tr_payment_setup_timezone();

  if ($_REQUEST["noBillingDateChange"]) {
    return;
  }

  if (!(uc_braintree_tr_payment_configuration_initialization())) {
    return;
  }

  try {
    $subscription = Braintree_Subscription::find($subscription_id);

    if ($subscription->_attributes['transactions']['0']->status <> 'settled') {
      uc_braintree_tr_payment_log_debug_message(t("The request may not be processed at this time. &nbsp;  The recurring payment is awaiting settlement and may not be credited at this time. &nbsp; Please contact an administrator if you believe you have received this message in error."));
      return;
    }
  }
  catch (Exception $e) {
    uc_braintree_tr_payment_connection_exception($e);
  }

  // Retrieve the original role ID.
  $subscriptions = uc_braintree_tr_payment_get_subscription_status($subscription_id);

  if (count($subscriptions) > 0) {
    foreach ($subscriptions as $subscription_id => $s_value) {
      $role_id = $s_value['roleId'];
      $order_id = $s_value['orderId'];
    }
  }

  uc_braintree_tr_payment_cancel_subscription($uid, $subscription_id, 0);

  $billing_day_of_month = $_REQUEST['billingDayOfMonth'];

  uc_braintree_tr_payment_log_debug_message(t("day of month: %billing_day_of_month",
      array('%billing_day_of_month', $billing_day_of_month)));

  $now = new DateTime("now");

  $nbd = new DateTime("now");
  $nbd->setDate($nbd->format('Y'), $nbd->format('m'), $billing_day_of_month);
  $nbd->setTime(0, 0, 0);

  if ($nbd > $now) {
    uc_braintree_tr_payment_log_debug_message(t("%next_billing_date occurs in future",
        array('%next_billing_date' => $nbd->format('Y-m-d'))));
  }
  else {
    uc_braintree_tr_payment_log_debug_message(t("%next_billing_date occurs in past",
        array('%next_billing_date' => $nbd->format('Y-m-d'))));

    // $nbd->modify("day of next month") did not work on all systems,
    // applying calendar logic.
    if ($nbd->format('m') + 1 > 12) {
      $month = 1;
      $year = $nbd->format('Y') + 1;
    }
    else {
      $month = $nbd->format('m') + 1;
      $year = $nbd->format('Y');
    }
    $nbd->setDate($year, $month, $nbd->format('d'));

  }

  $obd = new DateTime("now");
  $obd->setDate($subscription->nextBillingDate->format('Y'), $subscription->nextBillingDate->format('m'), $subscription->nextBillingDate->format('d'));
  $obd->setTime(0, 0, 0);

  uc_braintree_tr_payment_log_debug_message(t("payment method token: %token",
      array('%token' => $subscription->paymentMethodToken)));
  uc_braintree_tr_payment_log_debug_message(t("planId: %plan_id",
      array('%plan_id' => $subscription->planId)));
  uc_braintree_tr_payment_log_debug_message(t("price: %price",
      array('%price' => $subscription->price)));
  uc_braintree_tr_payment_log_debug_message(t("Old Billing Date: %old_billing_date",
      array('%old_billing_date' => $obd->format('Y-m-d'))));
  uc_braintree_tr_payment_log_debug_message(t("Next Billing Date: %next_billing_date",
      array('%next_billing_date' => $nbd->format('Y-m-d'))));

  try {
    $result = Braintree_Subscription::create(array(
        'paymentMethodToken' => $subscription->paymentMethodToken,
        'planId' => $subscription->planId,
        'firstBillingDate' => $nbd,
        'price' => $subscription->price,
    ));

    uc_braintree_tr_payment_save_customer_subscription($uid, $token, $order_id, $result->subscription->id, '', 0, $subscription->planId);

    // Remove any user role expiration if incorrectly configured in the product
    // feature.  Should always use "Never" for expiration with the Braintree
    // subscriptions. Rely on the hook_cron to remove roles when a credit card
    // has expired or the user has canceled a susbscription.
    $account = user_load($uid);

    uc_roles_delete($account, $role_id, TRUE);

    $new_subscription_id = $result->subscription->id;

    /*
     * Divide customer subscription price by average days per month (30.4375)
    * as `Daily Price`.
    *
    * Subtract days remaining of current subscription as `Credit Days`.
    *
    * Subtract days remaining between new bill date and now as `Charge Days`.
    *
    * Add (Negative (`Credit Days`)) and `Charge Days` as `Adjustment in Days`.
    *
    * If `Adjustment in Days` < 0, multiply `Adjustment in Days` by
    * `Daily Price` and apply as API Refund.
    *
    * If `Adjustment in Days` > 0, multiple `Adjustment in Days` by
    * `Daily Price` and apply as API Transaction.
    */

    $daily_price = $subscription->price / 30.4375;

    uc_braintree_tr_payment_log_debug_message(t("Daily Price: %daily_price",
        array('%daily_price' => $daily_price)));

    $credit_days = (int) round(($now->format('U') - $obd->format('U')) / (60 * 60 * 24));

    uc_braintree_tr_payment_log_debug_message(t("Credit Days: %credit_days",
        array('%credit_days' => $credit_days)));

    $charge_days = (int) round(($nbd->format('U') - $now->format('U')) / (60 * 60 * 24));

    uc_braintree_tr_payment_log_debug_message(t("Charge Days: %charge_days",
        array('%charge_days' => $charge_days)));

    $adjustment_in_days = (int) round($credit_days + $charge_days);

    if ($adjustment_in_days < 0) {
      if ($subscription->_attributes['transactions']['0']->id) {
        $amount = sprintf("%.2f", $adjustment_in_days * $daily_price * -1);

        uc_braintree_tr_payment_log_debug_message(t("Refund: %refund",
            array('%refund' => $amount)));

        $result = Braintree_Transaction::refund($subscription->_attributes['transactions']['0']->id, $amount);

        uc_braintree_tr_payment_log_debug_message(t("Refund success: %success",
            array('%success' => $result->success)));

        $refund = $result->transaction;

        uc_braintree_tr_payment_log_debug_message(t("Refund type: %type",
            array('%type' => $refund->type)));
        uc_braintree_tr_payment_log_debug_message(t("Original transaction id: %transaction_id",
            array('%transaction_id' => $refund->refundedTransactionId)));
        uc_braintree_tr_payment_log_debug_message(t("Refund Processor Response Code: %processor_code",
            array('%processor_code' => $result->transaction->processorResponseCode)));

      }
      else {
        uc_braintree_tr_payment_log_debug_message(t("A transaction id does not yet exist for this recurring payment"));
      }

    }
    elseif ($adjustment_in_days > 0) {
      $amount = sprintf("%.2f", $adjustment_in_days * $daily_price);

      uc_braintree_tr_payment_log_debug_message(t("Transaction: %amount",
          array('%amount' => $amount)));

      $result = Braintree_CreditCard::sale(
          $subscription->paymentMethodToken,
          array(
            'amount' => $amount,
            'options' => array(
              'submitForSettlement' => 'TRUE'),
          )
      );
    }

    db_query("UPDATE {uc_braintree_tr_payment_customer_subscription} set old_subscription_id = '%s',
        date_replaced = UNIX_TIMESTAMP(now()) where subscription_id = '%s'",
        $subscription_id, $new_subscription_id);
  }
  catch (Exception $e) {
    uc_braintree_tr_payment_connection_exception($e);
  }

}
